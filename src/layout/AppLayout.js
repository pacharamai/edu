import React from 'react'
import { Layout } from 'antd'
import AppSidebar from './AppSidebar';
import AppHeader from './AppHeader';
import AppContent from './AppContent';

const AppLayout = ({
  route
}) => {
  return (
    <Layout style={{ overflowX: 'hidden', maxHeight: '100vh', minHeight: '100vh' }}>
      <AppHeader />
      <Layout style={{ minWidth: 320 }}>
        <AppSidebar route={route} />
        <AppContent route={route} />
      </Layout>
    </Layout>
  )
}

export default AppLayout
