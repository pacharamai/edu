import React from 'react'
import { Layout } from 'antd'
const {
  Footer,
} = Layout
const AppFooter = () => {
  return (
    <Footer>
      {/* © Education Theme */}
    </Footer>
  )
}

export default AppFooter
