import React from 'react'
import { Tabs } from 'antd'

import List from './List'
import PageLayout from '../../layout/PageLayout'

const { TabPane } = Tabs

function callback(key) {
  console.log(key)
}

const Tab = () => (
  <Tabs defaultActiveKey='1' onChange={callback}>
    <TabPane tab='ผู้ป่วย' key='1'>
      <List />
    </TabPane>
    <TabPane tab='นักศึกษาแพทย์' key='2'>
      <PageLayout title='นักศึกษาแพทย์' />
    </TabPane>
  </Tabs>
)
export default Tab