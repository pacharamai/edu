import React, { useState, useEffect } from 'react'
import { Input, Button, Form } from 'antd'
import { useForm, Controller } from 'react-hook-form';
import PageLayout from '../../layout/PageLayout'


const FormDemo = () => {
  const { register, handleSubmit, setValue, control, errors } = useForm();
  const onSubmit = data => console.log(data);
  const handleChange = (e) => {
    setValue([e.target.name], e.target.value);
  }

  useEffect(() => {
    register({ name: 'name' }); // custom register react-select 
    register({ name: 'surname' }); // custom register antd input
  }, [register])

  return (
    <PageLayout title='Form With React hook form'>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Form.Item label="Name">
          <Input name='name' onChange={handleChange} />
        </Form.Item>
        <Form.Item label="Surname">
          <Input name='surname' onChange={handleChange} />
        </Form.Item>
        <Form.Item label="Ant Control">
          <Controller as={Input} name='antcontrol' control={control} defaultValue="" rules={{ required: true }} />
          {
            errors.antcontrol && <span>antconrol require</span>
          }
        </Form.Item>
        <Button htmlType='submit'>submit</Button>
      </form>
    </PageLayout>
  )
}

export default FormDemo