import React from 'react'
import PageLayout from '../../layout/PageLayout'
import EditorJs from 'react-editor-js'
import { EDITOR_JS_TOOLS } from '../../config/editor'


const Editor = () => {
  return (
    <PageLayout title='Editor'>
      <EditorJs
        tools={EDITOR_JS_TOOLS}
        data={{
          time: 1556098174501,
          blocks: [
            {
              type: "header",
              data: {
                text: "Editor.js",
                level: 2
              }
            },
            {
              type: "paragraph",
              data: {
                text:
                  "Hey. Meet the new Editor. On this page you can see it in action — try to edit this text."
              }
            },
            {
              type: "header",
              data: {
                text: "Key features",
                level: 3
              }
            },
            {
              type: "list",
              data: {
                style: "unordered",
                items: [
                  "It is a block-styled editor",
                  "It returns clean data output in JSON",
                  "Designed to be extendable and pluggable with a simple API"
                ]
              }
            }
          ]
        }}
      />
    </PageLayout>
  )
}

export default Editor