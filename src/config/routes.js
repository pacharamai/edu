import React from 'react'
// import { Redirect } from 'react-router-dom'
import Dashboard from "../pages/Dashboard";
import AppLayout from '../layout/AppLayout';
import Exception from '../pages/Exception';
import tableRoute from '../pages/Table/route'
import editorRoute from '../pages/Editor/route'
import formRoute from '../pages/Form/route'

const routes = [
  {
    component: AppLayout,
    routes: [
      {
        menu: 'Dashboard',
        path: "/",
        exact: true,
        component: Dashboard
      },
      ...tableRoute,
      ...editorRoute,
      ...formRoute
    ]
  },
  {
    component: () => (<Exception code="404" />)
  }
]
// export default routes;

const formattedRoute = routes.map(route => {
  if (route.routes) {
    route.path = route.routes.map(r => r.path)
  }
  return route
})

export default formattedRoute